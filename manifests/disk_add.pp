# Montage d'un stockage additionnel
class sys::disk_add (
  Hash[String, Hash] $mounts = {},
) {

  $mounts.each |String $dev, Hash $mount| {

    mounttab { $mount['dest']:
      ensure   => present,
      device   => $dev,
      fstype   => $mount['type'],
      options  => $mount['opts'],
      provider => augeas,
    }

    exec { $mount['dest']:
      command => "/bin/mkdir -p '${mount['dest']}' && /bin/mount '${mount['dest']}'",
      unless  => "/bin/mount -l | /bin/grep '${mount['dest']}'",
      require => Mounttab[$mount['dest']],
    }

    file { $mount['dest']:
      owner   => $mount['owner'],
      group   => $mount['group'],
      mode    => $mount['mode'],
      require => Exec[$mount['dest']],
    }

  }

}
