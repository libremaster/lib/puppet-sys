# Manager Letsencrypt ACME directory
define sys::letsencrypt_acme () {

  file {
    $name:
      ensure => directory,
      mode   => '0755',
      owner  => 'root';
    "${name}/.well-known":
      ensure  => directory,
      mode    => '0755',
      owner   => 'root',
      require => File[$name];
    "${name}/.well-known/acme-challenge":
      ensure  => directory,
      mode    => '0755',
      owner   => 'root',
      require => File["${name}/.well-known"];
  }

}
